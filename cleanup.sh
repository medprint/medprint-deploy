docker stop gitlab-container nginx-container postgresql-container redis-container django-container
docker rm gitlab-container nginx-container postgresql-container redis-container django-container
docker volume prune
rm -rf medprint-deploy medprint-frontend
