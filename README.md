# medprint-deploy

This project contains docker-compose to easily deploy your MedPrint instance.

# Prerequisites

1.  Docker and docker-compose

Working docker and docker-composose installation. Best practice is to allow non-root users: https://docs.docker.com/engine/install/linux-postinstall/

2.  Entries in hosts file

One entry for the GitLab and one for the Django frontend in `/etc/hosts`:

```
127.0.0.1 gitlab.medprint django.medprint postgresql.medprint
```

# Installation

Run:

```sh
./initMedprint.py
```

## Check Gitlab is ready

At some point in the script, you'll be asked to "Press Enter when Gitlab is ready"

To see if it's ready, run:

```
docker ps | grep gitlab
```

If it says "healthy" under "STATUS", you're good to go!

# Usage

You can find the root password in the terminal output of `initMedprint.py`

1.  Use the frontend: Go to http://django.medprint and sign in as root

2.  Use the backend: Go to http://gitlab.medprint and sign in as root

# Troubleshooting

## ListIndex out of range

This may come up if you're trying to restart the install process without removing previous data.

To fix this, we need to wipe out all that's come before.

### The Quick Fix

Be warned, at present this **deletes all docker volumes**. Not just medprint volumes, but **EVERYTHING!!!!**

```sh
./cleanup.sh
```

### The Better Way

```sh
docker stop gitlab-container nginx-container postgresql-container redis-container django-container
docker rm gitlab-container nginx-container postgresql-container redis-container django-container
```

Then:

```sh
docker volume ls
```

Remove the Docker volumes you don't want. Some of them may have cryptic names, so you may want to inspect them first:

```sh
docker volume rm <volume_name>
```

Delete the existing repository clones:

```sh
rm -rf medprint-deploy medprint-frontend
```
