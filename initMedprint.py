#!/usr/bin/python3
"""
Script that initializes the MedPrint system;
Tested with:
- MedPrint latest
"""
#import gitlab
import os
from base64 import b64encode
import sys
import lxml
import requests
from urllib.parse import urljoin
from bs4 import BeautifulSoup
import gitlab
from time import sleep


def create_django_env_file(postgresql_password, gitlab_url, gitlab_group_id):
    print("create_django_env_file")
    file_name = "django.env"
    random_bytes = os.urandom(24)
    django_secret_key = b64encode(random_bytes).decode('utf-8')
    variables = [
        "DEBUG=1",
        "SECRET_KEY="+django_secret_key,
        "DJANGO_ALLOWED_HOSTS=django.medprint localhost 127.0.0.1 [::1]",
        "SQL_ENGINE=django.db.backends.postgresql",
        "SQL_DATABASE=django",
        "SQL_USER=medprint",
        "SQL_PASSWORD="+postgresql_password,
        "SQL_HOST=medprint.postgresql",
        "SQL_PORT=5432",
        "GITLAB_HOST="+gitlab_url,
        "GITLAB_GROUP_ID="+str(gitlab_group_id)
    ]
    create_env_file(variables, file_name)


def create_gitlab_env_file(postgresql_password, gitlab_password):
    print("create_gitlab_env_file")
    file_name = "gitlab.env"
    random_bytes = os.urandom(24)
    gitlab_db_key_base = b64encode(random_bytes).decode('utf-8')
    random_bytes = os.urandom(24)
    gitlab_secret_key_base = b64encode(random_bytes).decode('utf-8')
    random_bytes = os.urandom(24)
    gitlab_otp_key_base = b64encode(random_bytes).decode('utf-8')
    variables = [
        "DEBUG=true",
        "DB_ADAPTER=postgresql",
        "DB_HOST=medprint.django",
        "DB_PORT=5432",
        "DB_USER=medprint",
        "DB_PASS="+postgresql_password,
        "DB_NAME=gitlab",
        "REDIS_HOST=redis",
        "REDIS_PORT=6379",
        "GITLAB_HTTPS=false",
        "SSL_SELF_SIGNED=false",
        "GITLAB_HOST=localhost",
        "GITLAB_PORT=80",
        "GITLAB_SSH_PORT=22",
        "GITLAB_ROOT_PASSWORD="+gitlab_password,
        "GITLAB_SECRETS_DB_KEY_BASE="+gitlab_db_key_base,
        "GITLAB_SECRETS_SECRET_KEY_BASE="+gitlab_secret_key_base,
        "GITLAB_SECRETS_OTP_KEY_BASE="+gitlab_otp_key_base
    ]
    create_env_file(variables, file_name)


def create_postgresql_env_file(postgresql_password):
    print("create_postgresql_env_file")
    file_name = "postgresql.env"
    variables = [
        "POSTGRES_USER=medprint",
        "POSTGRES_PASSWORD="+postgresql_password,
        "POSTGRES_DB=django"
    ]
    create_env_file(variables, file_name)


def create_env_file(variables, file_name):
    print("create_env_file")
    f = open(file_name, "w")
    for line in variables:
        f.write(line+"\n")
    f.close()


def append_compose_file():
    print("append_compose_file")
    file_name = "docker-compose.yml"
    lines = [
        "  django:",
        "    build: ../medprint-frontend/medprint",
        "    command: python manage.py runserver 0.0.0.0:8000",
        "    container_name: django-container",
        "    hostname: django.medprint",
        "    restart: always",
        "    networks:",
        "      web:",
        "        aliases:",
        "          - medprint.django",
        "      db:",
        "        aliases:",
        "          - medprint.django",
        "    expose:",
        "      - 8000",
        "    env_file:",
        "      - django.env",
        "    depends_on:",
        "      - postgresql",
        "      - gitlab",
        "    environment:",
        "      - VIRTUAL_HOST=django.medprint"
    ]
    f = open(file_name, "a")
    for line in lines:
        f.write(line+"\n")
    f.close()


def clone_frontend():
    print("clone_frontend")
    git_clone("https://gitlab.com/medprint/medprint-frontend.git")


def clone_deployment():
    print("clone_deployment")
    git_clone("https://gitlab.com/medprint/medprint-deploy.git")


def git_clone(url):
    print("git_clone")
    cmd = "git clone " + url
    os.system(cmd)


def docker_compose_up():
    print("docker_compose_up")
    cmd = "docker-compose up -d"
    os.system(cmd)


def find_csrf_token(text):
    soup = BeautifulSoup(text, "lxml")
    token = soup.find(attrs={"name": "csrf-token"})
    param = soup.find(attrs={"name": "csrf-param"})
    data = {param.get("content"): token.get("content")}
    return data


def obtain_csrf_token(endpoint):
    root_route = urljoin(endpoint, "/")
    r = requests.get(root_route)
    token = find_csrf_token(r.text)
    # print(dir(r))
    return token, r.cookies


def sign_in(endpoint, csrf, cookies, login, password):
    # print(f"Endpoint: {endpoint}")
    # print(f"csrf: {csrf}")
    # print(f"cookies: {cookies}")
    # print(f"login: {login}")
    # print(f"password: {password}")
    # password = 'adminadmin12345'
    data = {
        "user[login]": login,
        "user[password]": password,
        "user[remember_me]": 0,
        "utf8": "✓"
    }
    # print(f"Data: {data}")
    data.update(csrf)
    sign_in_route = urljoin(endpoint, "/users/sign_in")
    r = requests.post(sign_in_route, data=data, cookies=cookies)
    token = find_csrf_token(r.text)
    # print(f"Token: {token}")
    # print(f"r: {dir(r)}")
    # print(f"Cookies: {r.cookies}")
    # print(f"r.history: {r.history}")
    return token, r.history[0].cookies


def obtain_personal_access_token(endpoint, name, expires_at, csrf, cookies):
    data = {
        "personal_access_token[expires_at]": expires_at,
        "personal_access_token[name]": name,
        "personal_access_token[scopes][]": "api",
        "utf8": "✓"
    }
    data.update(csrf)
    pat_route = urljoin(endpoint, "/profile/personal_access_tokens")
    r = requests.post(pat_route, data=data, cookies=cookies)
    # print("-"*80)
    # print(f"data: {data}")
    # print(f"r: {r}")
    # print(f"r.text: {r.text}")
    # print("-"*80)
    soup = BeautifulSoup(r.text, "lxml")
    token = soup.find('input', id='created-personal-access-token').get('value')
    return token


def gitlab_get_access_token(endpoint, user, password):
    print("gitlab_get_access_token")
    name = "medprint_init"
    expires_at = "2020-04-30"
    csrf1, cookies1 = obtain_csrf_token(endpoint)
    csrf2, cookies2 = sign_in(endpoint, csrf1, cookies1, user, password)
    token = obtain_personal_access_token(
        endpoint, name, expires_at, csrf2, cookies2)
    print(token)
    return token


def gitlab_create_library(host, gitlab_token):
    print("gitlab_create_library")
    gl = gitlab.Gitlab(host, private_token=gitlab_token)

    # This deletes all previous groups, since we should start with clean slate
    # print("Deleting old Gitlab groups:")
    # all_groups = gl.groups.list()
    # for group in all_groups:
    # group.delete()
    # print(f"  Deleted {group.name}")

    # print("Waiting 20 seconds to ensure time for all old groups to be deleted")
    # sleep(20)

    group = gl.groups.create(
        {'name': 'library', 'path': 'library', 'visibility': 'public'})
    return group.id


def docker_compose_build():
    print("docker_compose_build")
    cmd = "docker-compose build"
    os.system(cmd)


def docker_start_django():
    print("docker_start_django")
    cmd = "docker-compose up -d django"
    os.system(cmd)


def django_migrate_db():
    print("django_migrate_db")
    cmd = "docker exec -it django-container ./manage.py migrate"
    os.system(cmd)


def django_create_superuser(username, password, gitlab_token):
    cmd = f"docker exec -it django-container ./manage.py superuser_from_gitlab {username} {password} {gitlab_token}"
    os.system(cmd)


# def show_instructions():
    # print("*"*80)
    # print("Instructions")
    # print("*"*80)
    # print(f"""
    # Username: {gitlab_user}
    # Password: {gitlab_password}
    # Gitlab Login: {gitlab_url}
    # Django Login: http://django.medprint/accounts/login/
    # """)


def main():
    gitlab_url = "http://gitlab.medprint"
    gitlab_user = "root"

    random_bytes = os.urandom(24)
    gitlab_password = b64encode(random_bytes).decode('utf-8')

    # Django is freaking out with special characters in passwords
    # gitlab_password = gitlab_password.replace('+', '_')
    # gitlab_password = gitlab_password.replace('/', '_')

    random_bytes = os.urandom(24)
    postgresql_password = b64encode(random_bytes).decode('utf-8')

    print("1")
    clone_frontend()
    clone_deployment()

    os.chdir('./medprint-deploy')

    print("2")
    create_gitlab_env_file(postgresql_password, gitlab_password)

    print("3")
    create_postgresql_env_file(postgresql_password)

    print("4")
    docker_compose_up()

    print("5")
    print("Press Enter when GitLab is ready...")
    continue_script = str(input())

    print("6")
    gitlab_token = gitlab_get_access_token(
        gitlab_url, gitlab_user, gitlab_password)
    gitlab_group_id = gitlab_create_library(gitlab_url, gitlab_token)

    print("7")
    append_compose_file()

    print("8")
    docker_compose_build()

    print("9")
    create_django_env_file(postgresql_password,
                           "http://gitlab", gitlab_group_id)

    print("10")
    docker_start_django()

    print("11")
    django_migrate_db()

    print("12")
    django_create_superuser(gitlab_user, gitlab_password, gitlab_token)
    # Create Django superuser w/ same credentials as gitlab, and enter their access token

    print("13")

    # show_instructions()

    print("*"*80)
    print("Instructions")
    print("*"*80)
    print(f"""
          Username: {gitlab_user}
          Password: {gitlab_password}
          Gitlab Login: {gitlab_url}
          Gitlab access token: {gitlab_token}
          Django Login: http://django.medprint/accounts/login/
          """)


if __name__ == "__main__":
    main()
